// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function string5(array) {
    if (array == undefined || array.length == 0 ) {
        return [];
    }
    let ansStr = "";
    for (let i = 0; i < array.length; i++) {
        if (array.length - 1 === i) {
            ansStr += array[i] + ".";
        }
        else {
            ansStr += array[i] + " ";
        }
    }
    return ansStr;
}
module.exports = string5;