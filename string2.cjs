// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function string2(str) {
    if (str == undefined || typeof str != "string" || str.length === 1 || str === "") {
        return [];
    }
    let ans = [];
    let regex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])$/ig;
    if (!regex.test(str)) {
        return [];
    }
    else {
        str.split(".").map((item) => {
            ans.push(Number(item));
        });
    }
    return ans;
}

module.exports = string2;
