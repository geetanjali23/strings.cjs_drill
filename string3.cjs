// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function string3(strInput) {
    if (strInput == undefined || strInput.length == 0) {
        return [];
    }
    let res = [];
    let monthArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    strInput.split("/").map((element, index) => {
        if (index === 1) {
            if (element < 1 || element > 12) {
                return res;
            }
            else {
                res.push(monthArr[Number(element) - 1]);
            }
        }
    });
    return res;
}
module.exports = string3;