// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

function string1(str) {
    if (str == undefined || typeof str != "string" || str.length === 1 || str === "") {
        return 0; //0 for invalid string
    }
    let resString = '';
    for (let i = 0; i < str.length; i++){
       if(str.charAt(i) === "$" || str.charAt(i) === "-" || str.charAt(i + 1) === "$" || str.charAt(i) === ",") {
           resString += "";
       }
       else {
            resString += str.charAt(i);
       }
    }
    return resString;
}
module.exports = string1;