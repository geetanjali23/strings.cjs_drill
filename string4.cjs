// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function string4(obj) {
    if (obj == undefined || typeof obj != "object" || Array.isArray(obj)) {
        return 0;
    }
    let fullName = "";
    for (let key in obj) {
        fullName += obj[key].slice(0, 1).toUpperCase() + obj[key].slice(1).toLowerCase() + " ";
    }
    return fullName;
}
module.exports = string4;